# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  config.vm.define "admin" do |admin|
    admin.vm.box = "centos/7"
    admin.vm.network "private_network", ip: "192.168.0.2"
    admin.vm.provision "shell", inline: "hostnamectl set-hostname admin"
    admin.vm.provision "file", source: "hosts_to_add", destination: "/tmp/hosts_to_add"
    admin.vm.provision "shell", inline: "cat /tmp/hosts_to_add >> /etc/hosts"
    admin.vm.provision "shell", inline: "yum install -y epel-release"
    admin.vm.provision "shell", inline: "yum install -y ansible"
    admin.vm.provision "file", source: "ansible_hosts", destination: "/tmp/ansible_hosts"
    admin.vm.provision "shell", inline: "cat /tmp/ansible_hosts >> /etc/ansible/hosts"    
    config.vm.provision "file", source: "id_rsa", destination: "/home/vagrant/.ssh/id_rsa"         # as user vagrant
    config.vm.provision "file", source: "id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub" 
    config.vm.provision "shell", inline: "chmod 600 /home/vagrant/.ssh/id_rsa"                     # as user root ~/ == /root
    config.vm.provision "shell", inline: "chmod 644 /home/vagrant/.ssh/id_rsa.pub"                
    config.vm.provision "shell", inline: "cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys"

    config.vm.provider "virtualbox" do |v|
      v.memory = 512
      v.cpus = 1
    end
  end

  config.vm.define "master" do |master|
    master.vm.box = "centos/7"
    master.vm.network "private_network", ip: "192.168.0.3"
    master.vm.provision "shell", inline: "hostnamectl set-hostname master"
    master.vm.provision "file", source: "hosts_to_add", destination: "/tmp/hosts_to_add"
    master.vm.provision "shell", inline: "cat /tmp/hosts_to_add >> /etc/hosts"
    config.vm.provision "file", source: "id_rsa", destination: "/home/vagrant/.ssh/id_rsa"         
    config.vm.provision "file", source: "id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub" 
    config.vm.provision "shell", inline: "chmod 600 /home/vagrant/.ssh/id_rsa"                     
    config.vm.provision "shell", inline: "chmod 644 /home/vagrant/.ssh/id_rsa.pub"                 
    config.vm.provision "shell", inline: "cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys"

    config.vm.provider "virtualbox" do |v|
      v.memory = 1024
      v.cpus = 1
    end
  end

  config.vm.define "slave1" do |slave1|
    slave1.vm.box = "centos/7"
    slave1.vm.network "private_network", ip: "192.168.0.4"
    slave1.vm.provision "shell", inline: "hostnamectl set-hostname slave1"
    slave1.vm.provision "file", source: "hosts_to_add", destination: "/tmp/hosts_to_add"
    slave1.vm.provision "shell", inline: "cat /tmp/hosts_to_add >> /etc/hosts"
    config.vm.provision "file", source: "id_rsa", destination: "/home/vagrant/.ssh/id_rsa"         
    config.vm.provision "file", source: "id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub" 
    config.vm.provision "shell", inline: "chmod 600 /home/vagrant/.ssh/id_rsa"                     
    config.vm.provision "shell", inline: "chmod 644 /home/vagrant/.ssh/id_rsa.pub"                 
    config.vm.provision "shell", inline: "cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys"

    config.vm.provider "virtualbox" do |v|
      v.memory = 1024
      v.cpus = 1
    end
  end

  config.vm.define "slave2" do |slave2|
    slave2.vm.box = "centos/7"
    slave2.vm.network "private_network", ip: "192.168.0.5"
    slave2.vm.provision "shell", inline: "hostnamectl set-hostname slave2"
    slave2.vm.provision "file", source: "hosts_to_add", destination: "/tmp/hosts_to_add"
    slave2.vm.provision "shell", inline: "cat /tmp/hosts_to_add >> /etc/hosts"
    config.vm.provision "file", source: "id_rsa", destination: "/home/vagrant/.ssh/id_rsa"         
    config.vm.provision "file", source: "id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub" 
    config.vm.provision "shell", inline: "chmod 600 /home/vagrant/.ssh/id_rsa"                     
    config.vm.provision "shell", inline: "chmod 644 /home/vagrant/.ssh/id_rsa.pub"                 
    config.vm.provision "shell", inline: "cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys"

    config.vm.provider "virtualbox" do |v|
      v.memory = 1024
      v.cpus = 1
    end
  end  
end